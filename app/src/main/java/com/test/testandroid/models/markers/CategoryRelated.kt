package com.test.testandroid.models.markers

/**
 *
 * @author Alexander Vilkov
 *
 * marker interface for the Category & Product entities
 */
interface CategoryRelated