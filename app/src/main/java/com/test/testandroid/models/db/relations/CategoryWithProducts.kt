package com.test.testandroid.models.db.relations

import androidx.room.Embedded
import androidx.room.Relation
import com.test.testandroid.models.Category
import com.test.testandroid.models.Product

/**
 *
 * @author Alexander Vilkov
 */
data class CategoryWithProducts(
    @Embedded val category: Category,
    @Relation(
        parentColumn = "id",
        entityColumn = "categoryId"
    )
    val products: List<Product>
)