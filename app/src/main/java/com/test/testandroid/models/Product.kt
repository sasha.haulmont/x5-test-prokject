package com.test.testandroid.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.test.testandroid.models.markers.CategoryRelated

@Entity(tableName = Product.TABLE_NAME)
data class Product(
    @PrimaryKey
    val id: Int,
    val name: String,
    val imageUrl: String,
    val categoryId: Int,
    val count: Int = 0
) : CategoryRelated {
    companion object {
        const val TABLE_NAME = "product"
    }
}