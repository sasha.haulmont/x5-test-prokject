package com.test.testandroid.models

import androidx.room.Entity

@Entity(primaryKeys = ["login"], tableName = User.TABLE_NAME)
data class User(
    val login: String,
    val password: String,
    val token: String
) {
    companion object {
        const val TABLE_NAME = "user"
    }
}