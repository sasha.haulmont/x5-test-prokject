package com.test.testandroid.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.test.testandroid.models.markers.CategoryRelated

@Entity(tableName = Category.TABLE_NAME)
data class Category(
    @PrimaryKey
    val id: Int,
    val categoryName: String
): CategoryRelated {
    companion object {
        const val TABLE_NAME = "category"
    }
}