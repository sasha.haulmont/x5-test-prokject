package com.test.testandroid.models.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.test.testandroid.models.Category
import com.test.testandroid.models.Product
import com.test.testandroid.models.User
import com.test.testandroid.models.db.dao.CategoryDao
import com.test.testandroid.models.db.dao.ProductDao
import com.test.testandroid.models.db.dao.UserDao

/**
 *
 * @author Alexander Vilkov
 */

@Database(
    entities = [
        User::class,
        Product::class,
        Category::class
    ],
    version = 2
)
abstract class TestDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun categoryDao(): CategoryDao
    abstract fun productDao(): ProductDao

    companion object {

        private const val DB_NAME = "test.db"

        private var instance: TestDatabase? = null

        fun getInstance(context: Context) = instance ?: synchronized(this) {
            buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context, TestDatabase::class.java, DB_NAME)
                .fallbackToDestructiveMigration()
                .build()
    }
}