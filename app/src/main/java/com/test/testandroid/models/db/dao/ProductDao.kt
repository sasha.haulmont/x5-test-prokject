package com.test.testandroid.models.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update
import com.test.testandroid.models.Product

/**
 *
 * @author Alexander Vilkov
 */

@Dao
interface ProductDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(products: List<Product>)

    @Update
    suspend fun update(product: Product)

}