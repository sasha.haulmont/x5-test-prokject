package com.test.testandroid.models.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.test.testandroid.models.User

/**
 *
 * @author Alexander Vilkov
 */

@Dao
interface UserDao {

    @Query("SELECT COUNT(*) FROM USER")
    suspend fun isAnyUserLoggedIn(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(user: User): Long

    @Query("SELECT * FROM USER LIMIT 1")
    suspend fun getUser(): User

}