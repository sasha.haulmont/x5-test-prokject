package com.test.testandroid.models.db.dao

import androidx.room.*
import com.test.testandroid.models.Category
import com.test.testandroid.models.db.relations.CategoryWithProducts
import kotlinx.coroutines.flow.Flow

/**
 *
 * @author Alexander Vilkov
 */

@Dao
interface CategoryDao {

    @Transaction
    @Query("SELECT * FROM CATEGORY")
    fun flowOfCategoryWithProduct(): Flow<List<CategoryWithProducts>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(categories: List<Category>)
}