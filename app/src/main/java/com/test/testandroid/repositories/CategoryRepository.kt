package com.test.testandroid.repositories

import com.test.testandroid.di.scopes.LoggedUserScope
import com.test.testandroid.models.Category
import com.test.testandroid.models.db.TestDatabase
import kotlinx.coroutines.delay
import javax.inject.Inject

@LoggedUserScope
class CategoryRepository @Inject constructor(private val db: TestDatabase) {

    val categoriesWithProducts = db.categoryDao().flowOfCategoryWithProduct()

    suspend fun loadCategories() {
        val user = db.userDao().getUser()
        val categories = getCategories(user.token)
        db.categoryDao().insert(categories)
    }

    private suspend fun getCategories(token: String): List<Category> {
        delay(1000)

        return listOf(
            Category(
                1,
                "Фрукты"
            ),
            Category(
                2,
                "Напитки"
            ),
            Category(
                3,
                "Бакалея"
            )
        )
    }
}