package com.test.testandroid.repositories

import com.test.testandroid.di.scopes.LoggedUserScope
import com.test.testandroid.models.Product
import com.test.testandroid.models.db.TestDatabase
import kotlinx.coroutines.delay
import javax.inject.Inject

@LoggedUserScope
class ProductRepository @Inject constructor(private val db: TestDatabase) {

    suspend fun loadProducts() {
        val user = db.userDao().getUser()
        val products = getProducts(user.token)
        db.productDao().insert(products)
    }

    suspend fun updateProduct(product: Product) {
        db.productDao().update(product)
    }

    private suspend fun getProducts(token: String): List<Product> {
        delay(1200)

        return listOf(
            Product(
                123,
                "Бананы сушеные",
                "/12/18/59",
                1
            ),
            Product(
                124,
                "Апельсины",
                "/11/73/82",
                1
            ),
            Product(
                125,
                "Лимон",
                "/10/91/14",
                1
            ),
            Product(
                126,
                "Вода",
                "/11/70/69",
                2
            ),
            Product(
                127,
                "Кола",
                "/10/00/86",
                2
            ),
            Product(
                128,
                "Спрайт",
                "/11/75/30",
                2
            ),
            Product(
                129,
                "Фанта",
                "/11/47/34",
                2
            ),
            Product(
                130,
                "Рис",
                "/10/29/51",
                3
            )
        )
    }
}