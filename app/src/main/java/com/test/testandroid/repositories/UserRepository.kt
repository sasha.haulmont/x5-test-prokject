package com.test.testandroid.repositories

import com.test.testandroid.models.User
import com.test.testandroid.models.db.TestDatabase
import javax.inject.Inject
import javax.inject.Singleton

/**
 *
 * @author Alexander Vilkov
 */
@Singleton
class UserRepository @Inject constructor(private val db: TestDatabase) {

    suspend fun checkIfAnyUserExists() = db.userDao().isAnyUserLoggedIn() > 0

    suspend fun loginUser(login: String, password: String): Boolean {
        return User(
            login,
            password,
            TOKEN
        ).let {
            db.userDao().insert(it) > 0
        }
    }

    companion object {
        // pretend like it rest-sourced token
        private const val TOKEN =
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3OTI3MDAwMDAwMCIsIm5hbWUiOiJUZXN0IFg1IFVzZXIiLCJpYXQiOjg0MTkzOTU3MX0.kQEKvbWnd1d0Qb1KlVzkWYG1MXEl8sgGr_HW_EznrbE"
    }

}