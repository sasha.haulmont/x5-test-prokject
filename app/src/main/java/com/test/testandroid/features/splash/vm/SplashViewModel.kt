package com.test.testandroid.features.splash.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.testandroid.di.scopes.AuthScope
import com.test.testandroid.features.splash.SplashActivity
import com.test.testandroid.repositories.UserRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 *
 * @author Alexander Vilkov
 */
@AuthScope
class SplashViewModel @Inject constructor(private val repo: UserRepository) : ViewModel() {

    private val _state = MutableLiveData<SplashActivity.SplashNavigationState>()
    val state: LiveData<SplashActivity.SplashNavigationState> get() = _state

    init {
        viewModelScope.launch {
            if (repo.checkIfAnyUserExists()) {
                SplashActivity.SplashNavigationState.MainState

            } else {
                SplashActivity.SplashNavigationState.AuthorizationState

            }.also { state ->
                _state.postValue(state)
            }
        }
    }

}