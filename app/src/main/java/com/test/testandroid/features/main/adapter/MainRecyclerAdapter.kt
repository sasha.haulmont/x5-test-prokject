package com.test.testandroid.features.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.test.testandroid.R
import com.test.testandroid.models.Category
import com.test.testandroid.models.Product
import com.test.testandroid.models.markers.CategoryRelated

class MainRecyclerAdapter(
    private val onProductUpdateListener: OnProductUpdateListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val data = mutableListOf<CategoryRelated>()

    fun updateData(data: List<CategoryRelated>) {
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int) =
        if (data[position] is Category) {
            R.layout.category_main_holder

        } else {
            R.layout.product_main_holder
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == R.layout.category_main_holder) {
            CategoryViewHolder(LayoutInflater.from(parent.context).inflate(viewType, parent, false))

        } else {
            ProductViewHolder(
                LayoutInflater.from(parent.context).inflate(viewType, parent, false),
                onProductUpdateListener
            )
        }
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CategoryViewHolder) {
            holder.bind(data[position] as Category)

        } else {
            (holder as ProductViewHolder).bind(data[position] as Product)
        }
    }

}

class OnProductUpdateListener(val onProductUpdate: (product: Product) -> Unit) {
    fun onButtonClick(product: Product) = onProductUpdate(product)
}