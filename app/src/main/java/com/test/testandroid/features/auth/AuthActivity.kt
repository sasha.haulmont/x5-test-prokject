package com.test.testandroid.features.auth

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.test.testandroid.R
import com.test.testandroid.TestApplication
import com.test.testandroid.features.auth.vm.AuthViewModel
import com.test.testandroid.features.main.MainActivity
import kotlinx.android.synthetic.main.activity_auth.*
import javax.inject.Inject

class AuthActivity : AppCompatActivity() {

    @Inject lateinit var viewModel: AuthViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        TestApplication.instance.appComponent.authComponent().create().inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        viewModel.authState.observe(this) { state ->
            when (state) {
                is AuthNavigationState.AuthSuccess -> {
                    intentToMainActivity()
                }
                is AuthNavigationState.AuthFailed -> Unit
            }
        }

        authButton.setOnClickListener { loginClick() }
    }

    private fun loginClick() {
        val login = loginAuthEditText.text.toString()
        val password = passwordAuthEditText.text.toString()
        if (login.isNotEmpty() && password.isNotEmpty()) {
            viewModel.login(login, password)

        } else {
            Toast.makeText(this, "Заполните все поля", Toast.LENGTH_SHORT).show()
        }
    }

    private fun intentToMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    sealed class AuthNavigationState {
        object AuthSuccess: AuthNavigationState()
        object AuthFailed: AuthNavigationState() // not used, nice to have
    }
}