package com.test.testandroid.features.main.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.testandroid.di.scopes.LoggedUserScope
import com.test.testandroid.features.main.MainActivity
import com.test.testandroid.models.Product
import com.test.testandroid.models.markers.CategoryRelated
import com.test.testandroid.repositories.CategoryRepository
import com.test.testandroid.repositories.ProductRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 *
 * @author Alexander Vilkov
 */

@LoggedUserScope
class MainViewModel @Inject constructor(
    private val categoryRepo: CategoryRepository,
    private val productRepo: ProductRepository
) : ViewModel() {

    private val _categoryLoadState = MutableLiveData<MainActivity.CategoryLoadState>()
    val categoryLoadState: LiveData<MainActivity.CategoryLoadState> get() = _categoryLoadState

    init {
        viewModelScope.launch {
            productRepo.loadProducts()
            categoryRepo.loadCategories()

            categoryRepo.categoriesWithProducts.map { categoriesWithProducts ->
                if (categoriesWithProducts.isNullOrEmpty()) {
                    MainActivity.CategoryLoadState.Failure

                } else {
                    categoriesWithProducts.flatMap { elem ->
                        mutableListOf<CategoryRelated>().apply {
                            add(elem.category)
                            addAll(elem.products)
                        }

                    }.let { data ->
                        MainActivity.CategoryLoadState.Success(data)
                    }
                }

            }.collect { state ->
                _categoryLoadState.postValue(state)
            }
        }
    }

    fun updateProduct(product: Product) {
        viewModelScope.launch {
            productRepo.updateProduct(product)
        }
    }
}