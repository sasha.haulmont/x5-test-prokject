package com.test.testandroid.features.auth.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.testandroid.features.auth.AuthActivity
import com.test.testandroid.repositories.UserRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 *
 * @author Alexander Vilkov
 */


class AuthViewModel @Inject constructor(private val repo: UserRepository) : ViewModel() {

    private val _authState = MutableLiveData<AuthActivity.AuthNavigationState>()
    val authState: LiveData<AuthActivity.AuthNavigationState> get() = _authState

    fun login(login: String, password: String) {
        viewModelScope.launch {
            if (repo.loginUser(login, password)) {
                AuthActivity.AuthNavigationState.AuthSuccess

            } else {
                AuthActivity.AuthNavigationState.AuthFailed

            }.also {  state ->
                _authState.postValue(state)
            }
        }
    }

}