package com.test.testandroid.features.main.adapter

import android.view.View
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.test.testandroid.loadImage
import com.test.testandroid.models.Product
import kotlinx.android.synthetic.main.product_main_holder.view.*

class ProductViewHolder(
    itemView: View,
    private val onProductUpdateListener: OnProductUpdateListener
) : RecyclerView.ViewHolder(itemView) {
    fun bind(product: Product) = with(itemView) {
//        loadImage(context, productImageView, product.imageUrl)

        Picasso.with(context)
            .load("https://photos.5-delivery.ru/small${product.imageUrl}")
            .networkPolicy(NetworkPolicy.NO_CACHE)
            .into(productImageView)

        productCountTextView.apply {
            isInvisible = product.count == 0
            text = product.count.toString()
        }

        plusButton.apply {
            isInvisible = product.count == 0
            setOnClickListener {
                onProductUpdateListener.onButtonClick(product.copy(count = product.count + 1))
            }
        }

        minusButton.apply {
            isInvisible = product.count == 0
            setOnClickListener {
                onProductUpdateListener.onButtonClick(product.copy(count = product.count - 1))
            }
        }

        addToBasketButton.apply {
            isInvisible = product.count > 0
            setOnClickListener {
                onProductUpdateListener.onButtonClick(product.copy(count = 1))
            }
        }
        productNameTextView.text = product.name
    }
}