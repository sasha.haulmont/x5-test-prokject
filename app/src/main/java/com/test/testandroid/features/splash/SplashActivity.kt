package com.test.testandroid.features.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.test.testandroid.TestApplication
import com.test.testandroid.features.auth.AuthActivity
import com.test.testandroid.features.main.MainActivity
import com.test.testandroid.features.splash.vm.SplashViewModel
import javax.inject.Inject

class SplashActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModel: SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        TestApplication.instance.appComponent.authComponent().create().inject(this)
        super.onCreate(savedInstanceState)

        viewModel.state.observe(this) { state ->
            when (state) {
                is SplashNavigationState.AuthorizationState -> {
                    intentTo(AuthActivity::class.java)
                }
                is SplashNavigationState.MainState -> {
                    intentTo(MainActivity::class.java)
                }
            }
        }
    }

    private fun intentTo(act: Class<out AppCompatActivity>) {
        val intent = Intent(this, act)
        startActivity(intent)
        finish()
    }

    sealed class SplashNavigationState {
        object AuthorizationState : SplashNavigationState()
        object MainState : SplashNavigationState()
    }

}