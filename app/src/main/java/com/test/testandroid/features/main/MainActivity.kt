package com.test.testandroid.features.main

import android.os.Bundle
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.asLiveData
import androidx.recyclerview.widget.RecyclerView
import com.test.testandroid.R
import com.test.testandroid.TestApplication
import com.test.testandroid.features.main.adapter.MainRecyclerAdapter
import com.test.testandroid.features.main.adapter.OnProductUpdateListener
import com.test.testandroid.features.main.vm.MainViewModel
import com.test.testandroid.models.markers.CategoryRelated
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModel: MainViewModel

    private val adapter: MainRecyclerAdapter?
        get() = (recyclerView.adapter as? MainRecyclerAdapter)
    private lateinit var recyclerView: RecyclerView
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        TestApplication.instance.appComponent.loggedUserComponent().create().inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        progressBar = findViewById(R.id.mainProgressBar)
        findViewById<RecyclerView>(R.id.mainRecyclerView).apply {
            adapter = MainRecyclerAdapter(
                OnProductUpdateListener(
                    onProductUpdate = { product ->
                        viewModel.updateProduct(product)
                    }
                )
            )

        }.also {
            recyclerView = it
        }

        viewModel.categoryLoadState.observe(this) { state ->

            when (state) {
                is CategoryLoadState.Loading -> {
                    progressBar.isVisible = true
                    recyclerView.isVisible = false
                }
                is CategoryLoadState.Success -> {
                    progressBar.isVisible = false
                    recyclerView.isVisible = true

                    adapter?.updateData(state.data)
                }
                is CategoryLoadState.Failure -> Unit
            }
        }
    }

    sealed class CategoryLoadState {
        object Loading: CategoryLoadState()
        class Success(val data: List<CategoryRelated>) : CategoryLoadState()
        object Failure : CategoryLoadState() // nice to have
    }

}
