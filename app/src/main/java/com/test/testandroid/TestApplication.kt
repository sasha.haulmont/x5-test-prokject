package com.test.testandroid

import android.app.Application
import com.squareup.picasso.Picasso
import com.test.testandroid.di.components.AppComponent
import com.test.testandroid.di.components.DaggerAppComponent

class TestApplication : Application() {

    val appComponent: AppComponent = DaggerAppComponent.create()

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        lateinit var instance: TestApplication
    }
}