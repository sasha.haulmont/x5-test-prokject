package com.test.testandroid.di.scopes

import javax.inject.Scope

/**
 *
 * @author Alexander Vilkov
 */

@Scope
@Retention(value = AnnotationRetention.RUNTIME)
annotation class LoggedUserScope
