package com.test.testandroid.di.components

import com.test.testandroid.di.scopes.AuthScope
import com.test.testandroid.features.auth.AuthActivity
import com.test.testandroid.features.splash.SplashActivity
import dagger.Subcomponent

/**
 *
 * @author Alexander Vilkov
 */

@AuthScope
@Subcomponent
interface AuthComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): AuthComponent
    }

    fun inject(splashActivity: SplashActivity)
    fun inject(authActivity: AuthActivity)

}