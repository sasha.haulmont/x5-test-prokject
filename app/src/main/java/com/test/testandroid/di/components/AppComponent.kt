package com.test.testandroid.di.components

import com.test.testandroid.di.modules.DbModule
import com.test.testandroid.di.modules.SubcomponentsModule
import dagger.Component
import javax.inject.Singleton

/**
 *
 * @author Alexander Vilkov
 */

@Singleton
@Component(modules = [DbModule::class, SubcomponentsModule::class])
interface AppComponent {
    fun authComponent(): AuthComponent.Factory
    fun loggedUserComponent(): LoggedUserComponent.Factory
}