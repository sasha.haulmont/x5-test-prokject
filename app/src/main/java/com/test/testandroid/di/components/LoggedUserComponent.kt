package com.test.testandroid.di.components

import com.test.testandroid.di.scopes.LoggedUserScope
import com.test.testandroid.features.main.MainActivity
import dagger.Subcomponent

/**
 *
 * @author Alexander Vilkov
 */

@LoggedUserScope
@Subcomponent
interface LoggedUserComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): LoggedUserComponent
    }

    fun inject(mainActivity: MainActivity)

}