package com.test.testandroid.di.modules

import com.test.testandroid.TestApplication
import com.test.testandroid.models.db.TestDatabase
import dagger.Module
import dagger.Provides

/**
 *
 * @author Alexander Vilkov
 */
@Module
class DbModule {

    @Provides
    fun provideDb() = TestDatabase.getInstance(TestApplication.instance)
}