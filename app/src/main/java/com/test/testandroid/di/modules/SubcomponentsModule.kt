package com.test.testandroid.di.modules

import com.test.testandroid.di.components.AuthComponent
import com.test.testandroid.di.components.LoggedUserComponent
import dagger.Module

/**
 *
 * @author Alexander Vilkov
 */

@Module(subcomponents = [AuthComponent::class, LoggedUserComponent::class])
class SubcomponentsModule